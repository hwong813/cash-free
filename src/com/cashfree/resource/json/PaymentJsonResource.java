package com.cashfree.resource.json;

import com.googlecode.objectify.Key;
import com.cashfree.entity.Payment;
import com.cashfree.gson.GsonFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Path("/json/payment")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PaymentJsonResource {

    @GET
    @Path("/get")
    public String get(@QueryParam("id") String id) {
        Payment payment = ofy().load().key(Key.create(Payment.class, Long.parseLong(id))).get();
        String json = GsonFactory.gson().toJson(payment);
        return json;
    }

    @GET
    @Path("/list")
    public String list() {
        List<Payment> paymentList = ofy().load().type(Payment.class).order("-timestamp").list();
        String json = GsonFactory.gson().toJson(paymentList);
        return json;
    }

    @POST
    @Path("/add")
    public String add(String paymentStr) {
        Payment payment = GsonFactory.gson().fromJson(paymentStr, Payment.class);
        System.out.println("Adding payment " + payment);
        ofy().save().entity(payment);

        return JsonCommon.RESULT_OK;
    }
}

